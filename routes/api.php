<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//Testing purpose
// -------------------------------------------------------------
Route::get('index','TestController@index');
// Route::get('user','TestController@index');
Route::post('insert','TestController@insert');
// -------------------------------------------------------------

Route::post('login', 'API\UserApiController@login');
Route::post('register', 'API\UserApiController@register');

Route::group(['middleware' => 'auth:api'], function(){
	Route::post('details', 'API\UserApiController@details');
	Route::post('category','API\CategoryApiController@store');
	Route::post('budget','API\BudgetApiController@store');

	Route::get('expense/category','API\ExpenseApiController@getCategory');
	Route::get('expense/budget','API\ExpenseApiController@getBudget');
	Route::post('expense','API\ExpenseApiController@store');
	Route::get('expenses','API\ExpenseApiController@index');
	Route::put('expense/{id}', 'API\ExpenseApiController@update');
	Route::delete('expense/{id}', 'API\ExpenseApiController@destroy');
	Route::get('expense/{id}', 'API\ExpenseApiController@show');

	Route::post('income','API\IncomeApiController@store');
	Route::get('incomes','API\IncomeApiController@index');
	Route::put('income/{id}', 'API\IncomeApiController@update');
	Route::delete('income/{id}', 'API\IncomeApiController@destroy');
	Route::get('income/{id}', 'API\IncomeApiController@show');


	Route::post('setting/changeUsername','API\SettingApiController@changeUsername');
	Route::post('setting/changePassword','API\SettingApiController@changePassword');
	Route::get('user','API\SettingApiController@getUser');
	

	Route::get('totalexpense','API\ReportApiController@getAllData');
	// ------------------------------------------------------------
	Route::post('plan','API\ReportApiController@getUserPlan');
	Route::get('budget','API\ReportApiController@getBudgetLeft');
	Route::get('month','API\ReportApiController@getTotalExpenseMonth');

	Route::get('all','API\ReportApiController@getAll');
	Route::get('users/details','API\UserApiController@getUserDetails');
	Route::get('allplan','API\ReportApiController@getAllPlan');
	Route::get('sort','API\ExpenseApiController@sort');
	Route::get('summaryreport','API\ReportApiController@summaryreport');
});