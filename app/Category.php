<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    //

	protected $fillable = [
		'user_id',
		'type',
	];

	// public function budget()
	// {
	// 	return $this->hasOne(Budget::class);
	// }

	public function expenses()
	{
		return $this->hasMany(Expense::class);
	}

	public function user()
	{
		return $this->belongsTo(User::class);
	}
}
