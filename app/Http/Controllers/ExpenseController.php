<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Expense;
use Carbon\Carbon;

class ExpenseController extends Controller
{
    //
    public function create()
	{
		
	}
	
	public function store(Request $request)
	{
		$this->validate($request,[
			'budget_id'=>'required',
			'category_id'=>'required',
			'amount' =>'required',
			'date'  =>'required',
			'description' => 'required',
		]);
		$expense = new Expense;
		// $expense->category = $request->input('category');
		$expense->budget_id = $request->input('budget_id');
		$expense->category_id = $request->input('category_id');
		$expense->amount = $request->input('amount');
		$expense->date = Carbon::parse($request->input('date'));
		$expense->description = $request->input('description');
		$expense->user_id = auth()->id();
		
		$expense->save();
		return redirect('expense')->with('success','Data Saved');
	}
	
	public function index()
	{
		$expenses = Expense::orderBy('date', 'desc')->get();
		
		return view('expense', [
		'expenses' => $expenses
		]);
		// return view('expense');
	}
	
	public function show($id)
	{
		
	}
	
	public function edit($id)
	{
	
	}
	
	public function update(Request $request, $id)
	{
		$this->validate($request,[
			'category_id'=>'required',
			'budget_id' => 'required',
			'amount' =>'required',
			'date'  =>'required',
			'description' => 'required',
		]);
		
		$expense = Expense::find($id);
		// if(!$expense) throw new ModelNotFoundException;
		$expense->category_id = $request->input('category_id');
		$expense->budget_id = $request->input('budget_id');
		$expense->amount = $request->input('amount');
		$expense->date = Carbon::parse($request->input('date'));
		$expense->description = $request->input('description');

		$expense->save();
		return redirect('expense')->with('success','Data Updated');
	}

	public function destroy($id)
	{
		$expense = Expense::find($id);
		// $expense =Expense::where('id',$id)->first();
		if($expense != null){
		$expense->delete();
		
		return redirect('expense')->with('success','Data has been deleted');
		}

	}
}
