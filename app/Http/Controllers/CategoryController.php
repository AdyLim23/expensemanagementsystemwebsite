<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;

class CategoryController extends Controller
{
    //
    public function store(Request $request)
	{
		$this->validate($request,[
			'category' =>'required',
		]);
		$category = new Category;
		$category->type = $request->input('category');
		$category->user_id = auth()->id();
		$category->save();
		return redirect('home')->with('success','Category Added');
	}
}
