<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Expense;
use App\Income;

class ReportController extends Controller
{
    //

	public function index()
	{
		$expenses = Expense::orderBy('date', 'desc')->get();
        // $expenses = $expenses->sum('amount');
		$incomes = Income::all();
		return view('monthlyReportContent', [
			'expenses' => $expenses,
			'incomes' => $incomes,
		]);

        // return view('home');
	}

	public function show(Request $request)
	{
		$yearFilter = $request->input('year');
		return view('home',compact('yearFilter'));
	}

	public function reportShow(Request $request)
	{
		$yearFilter = $request->input('year');
		return view('monthlyReportContent',compact('yearFilter'));
	}
}
