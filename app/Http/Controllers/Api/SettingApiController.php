<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller; 
use App\User; 
use Validator;
use Hash;
use Illuminate\Support\Facades\Auth;
class SettingApiController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');

    }

    public function changePassword(Request $request){

    $input = $request->all();
    $userid = Auth::guard('api')->user()->id;
    $rules = array(
        'old_password' => 'required',
        'new_password' => 'required|min:6',
        'confirm_password' => 'required|same:new_password',
    );
    $validator = Validator::make($input, $rules);
    if ($validator->fails()) {
        $arr = array("status" => 400, "message" => $validator->errors()->first(), "data" => array());
    } else {
        try {
            if ((Hash::check(request('old_password'), Auth::user()->password)) == false) {
                $arr = array("status" => 400, "message" => "Check your old password.", "data" => array());
            } else if ((Hash::check(request('new_password'), Auth::user()->password)) == true) {
                $arr = array("status" => 400, "message" => "Please enter a password which is not similar then current password.", "data" => array());
            } else {
                User::where('id', $userid)->update(['password' => Hash::make($input['new_password'])]);
                $arr = array("status" => 200, "message" => "Password updated successfully.", "data" => array());
            }
        } catch (\Exception $ex) {
            if (isset($ex->errorInfo[2])) {
                $msg = $ex->errorInfo[2];
            } else {
                $msg = $ex->getMessage();
            }
            $arr = array("status" => 400, "message" => $msg, "data" => array());
        }
    }
    return \Response::json($arr);

    }
    public function changeUsername(Request $request){

        //Change Username
        $user = Auth::user();
        $user->name = ($request->input('new_name'));
        $result = $user->save();

        if($result == 1){
            return response()->json(['data'=> $result],200);
        }else{
             return response()->json(['Error'=> 'No Such Data'],404);
        }

    }

    public function getUser(){

        //Change Username
        $user = Auth::user();
        

        if($user){
            return response()->json(['data'=> $user],200);
        }else{
             return response()->json(['Error'=> 'No Such Data'],404);
        }

    }

}
