<?php

namespace App\Http\Controllers\API;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Category;
use App\Budget;
use App\Expense;
use Validator;
use Illuminate\Support\Facades\Input;
use Carbon\Carbon;
use App\Income;

class IncomeApiController extends Controller
{
    //
	public function index()
	{
		$incomes = Income::orderBy('date', 'desc')->get();
		
		if($incomes)
		{
			return response()->json(['data'=>$incomes],200); 
		}
		else
		{
			return response()->json(['error'=>'No Such Data'], 401);
		}
		// return view('expense');
	}

    public function store(Request $request)
	{
		$validator = Validator::make($request->all(), [ 
			'amount' =>'required',
            'date'  =>'required',
            'description' => 'required',
		]);

		if ($validator->fails()) { 
			return response()->json(['error'=>$validator->errors()], 401);            
		}

		$income = new Income;
        $income->amount = $request->input('amount');
        $income->date = Carbon::parse($request->input('date'));
        $income->description = $request->input('description');
        $income->user_id = auth()->id();
		
		$result = $income->save();

		if($result == 1){
			return response()->json(['success'=> $result],200);
		}

	}

	//haveent test yet
	public function destroy($id)
	{
		$income = Income::find($id);
		// $expense =Expense::where('id',$id)->first();
		if($income != null){
		$income->delete();
		
		return response()->json(['success'=> $income],200);
		}

	}

	public function update(Request $request, $id)
	{
		$validator = Validator::make($request->all(), [ 
			'amount' =>'required',
            'date'  =>'required',
            'description' => 'required',

		]);

		if ($validator->fails()) { 
			return response()->json(['error'=>$validator->errors()], 401);            
		}
		
	    $income = Income::find($id);
        // if(!$expense) throw new ModelNotFoundException;
        $income->amount = $request->input('amount');
        $income->date = Carbon::parse($request->input('date'));
        $income->description = $request->input('description');
		// $expense->user_id = auth()->id();

		$result = $income->save();

		if($result == 1){
			return response()->json(['success'=> $result],200);
		}
	}

	public function show($id)
	{
		$income = Income::find($id);
	   	if($income)
    	{
    		return response()->json(['data'=>$income],200);
    	}else{
    		return response()->json(['error'=>'No Such Data'], 401);
    	}
	}
}
