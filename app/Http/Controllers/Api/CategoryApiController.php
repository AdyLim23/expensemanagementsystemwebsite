<?php

namespace App\Http\Controllers\API;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Category;
use Validator;
use Illuminate\Support\Facades\Input;
class CategoryApiController extends Controller
{
    //
    public function store(Request $request)
	{
		// $input = $request->json()->all();
		// $category = new Category($input);
		// $category->user_id = auth()->id();
		// $category->type = $input;
		// --------------------------------------------
		$validator = Validator::make($request->all(), [ 
			'category' =>'required', 
			// 'user_id' => auth()->id(),
		]);

		// $this->validate($request,[
		// 	'category' =>'required',
		// ]);
		if ($validator->fails()) { 
			return response()->json(['error'=>$validator->errors()], 401);            
		}

		$category = new Category();
		$category->type = $request->input('category');
		$category->user_id = auth()->id();
		$result = $category->save();
		if($result == 1){
			return response()->json(['success'=> $result],200);
		}
		// --------------------------------------------
		// $input->fill(['user_id' => auth()->id()]);


		// $category = new Category;


		// $input = $request->except('user_id');
		// $input->fill(['user_id' => 1]);
		// $category = Category::create($input);	

		// $category = new Category;
		// $category->type = Input::get('type');

		// $category->save();


		// return response()->json(['success'=>$input], 200);
	}
}
