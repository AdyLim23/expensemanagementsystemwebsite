<?php

namespace App\Http\Controllers\API;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Category;
use App\Budget;
use App\Expense;
use App\Income;
use Validator;
use Illuminate\Support\Facades\Input;
use Carbon\Carbon;


class ReportApiController extends Controller
{
    //
    private $userPlan;

	public function index()
	{
		$expenses = Expense::orderBy('date', 'desc')->get();
        // $expenses = $expenses->sum('amount');
		$incomes = Income::all();
		return view('monthlyReportContent', [
			'expenses' => $expenses,
			'incomes' => $incomes,
		]);

        // return view('home');
	}

	public function show(Request $request)
	{
		$yearFilter = $request->input('year');
		return view('home',compact('yearFilter'));
	}

	public function reportShow(Request $request)
	{
		$yearFilter = $request->input('year');
		return view('monthlyReportContent',compact('yearFilter'));
	}
	// -------------------------------------------------------------------
	public function getAllData(){
		$totalExpense = Expense::all()->sum('amount');
		$totalIncome = Income::all()->sum('amount');
		$totalGap = $totalIncome - $totalExpense;
		return response()->json([
			'expense' => $totalExpense,
			'income' => $totalIncome,
			'difference' => $totalGap
			],200);
	}
    public function getUserPlan(Request $request){
        $this->userPlan = $request->input('budget');
        // $result = $this->userPlan->save();
        return response()->json(['success'=> $this->userPlan],200);
    }

	public function getBudgetLeft(Request $request){
		$userPlan = $request->input('budget');
        // return response()->json(['success'=>$this->userPlan],200); 
		// to get budget month
        // $userPlan = $this->userPlan;
        // $userPlan = 1;
        if(!($userPlan))
        {
            $userPlan = 1;
            $typePlan = null;

            $try = Budget::where('id',$userPlan)->first();
            //to get all the expenses belongs to same budget plan
            $try2 = Expense::where('budget_id',$userPlan)->get();



            //to get the budget table data according to which plan
            $budgetCategory = Budget::where('id',$userPlan)->first();
            //to get the expense table data according to budget plan
            $category = Expense::where('budget_id',$userPlan)->get();


            //Total expenses of same plan in different categories
            if($category->where('category_id',1)){
                $categoryExpenseLivings = $category->where('category_id',1)->sum('amount');
            }
            if($category->where('category_id',2)){
                $categoryExpenseOthers = $category->where('category_id',2)->sum('amount');
            }
            if($category->where('category_id',3)){
                $categoryExpenseSavings = $category->where('category_id',3)->sum('amount');
            }



            if($budgetCategory->typePlan == '50-30-20'){
                $budget50Livings = ($budgetCategory->budgetMonth)/(2);
                $budget30Others = ($budgetCategory->budgetMonth)*(30/100);
                $budget20Savings = ($budgetCategory->budgetMonth)*(20/100);


            }else if($budgetCategory->typePlan == '75-15-10'){
                $budget50Livings = ($budgetCategory->budgetMonth)*(75/100);
                $budget30Others = ($budgetCategory->budgetMonth)*(15/100);
                $budget20Savings = ($budgetCategory->budgetMonth)*(10/100);


            }else if($budgetCategory->typePlan == '60-10-30'){
                $budget50Livings = ($budgetCategory->budgetMonth)*(60/100);
                $budget30Others = ($budgetCategory->budgetMonth)*(10/100);
                $budget20Savings = ($budgetCategory->budgetMonth)*(30/100);


            }else{
                $budget50Livings = $budgetCategory->livingsBudget;
                $budget30Others = $budgetCategory->othersBudget;
                $budget20Savings = $budgetCategory->savingsBudget;    
            }
        }
        else{

            $try = Budget::where('id',$userPlan)->first();
            $try2 = Expense::where('budget_id',$userPlan)->get();

            $budgetCategory = Budget::where('id',$userPlan)->first();

            $category = Expense::where('budget_id',$userPlan)->get();

            if($category->where('category_id',1)){
                $categoryExpenseLivings = $category->where('category_id',1)->sum('amount');
            }
            if($category->where('category_id',2)){
                $categoryExpenseOthers = $category->where('category_id',2)->sum('amount');
            }
            if($category->where('category_id',3)){
                $categoryExpenseSavings = $category->where('category_id',3)->sum('amount');
            }





            if($budgetCategory->typePlan == '50-30-20'){
                $budget50Livings = ($budgetCategory->budgetMonth)/(2);
                $budget30Others = ($budgetCategory->budgetMonth)*(30/100);
                $budget20Savings = ($budgetCategory->budgetMonth)*(20/100);


            }else if($budgetCategory->typePlan == '75-15-10'){
                $budget50Livings = ($budgetCategory->budgetMonth)*(75/100);
                $budget30Others = ($budgetCategory->budgetMonth)*(15/100);
                $budget20Savings = ($budgetCategory->budgetMonth)*(10/100);


            }else if($budgetCategory->typePlan == '60-10-30'){
                $budget50Livings = ($budgetCategory->budgetMonth)*(60/100);
                $budget30Others = ($budgetCategory->budgetMonth)*(10/100);
                $budget20Savings = ($budgetCategory->budgetMonth)*(30/100);


            }else{
                $budget50Livings = $budgetCategory->livingsBudget;
                $budget30Others = $budgetCategory->othersBudget;
                $budget20Savings = $budgetCategory->savingsBudget;    
            }


        }
        return response()->json([
            'budgetPlan' => $try,
            'expense' => $try2,
            'expenseLivings' => $categoryExpenseLivings,
            'expenseOthers' => $categoryExpenseOthers,
            'expenseSavings' => $categoryExpenseSavings,
            'budgetLivings' => $budget50Livings,
            'budgetOthers' => $budget30Others,
            'budget20Savings' => $budget20Savings]
            ,200);
    
    }

    public function getAll(){
        $budget = Budget::all();
        // $category = Category::all();
        $expense = Expense::all();
         return response()->json([
            'budget' => $budget,
            'expense' => $expense]
            ,200);
    }

    public function getTotalExpenseMonth(Request $request){
        $yearFilter = $request->input('year');
        if(empty($yearFilter)) {

            $yearFilter = '2020';

            $totalExpenseMonth1 = Expense::whereMonth('date','01')->whereYear('date',$yearFilter)->get();
            $jan = $totalExpenseMonth1->sum('amount');

            $totalExpenseMonth2 = Expense::whereMonth('date','02')->whereYear('date',$yearFilter)->get();
            $feb = $totalExpenseMonth2->sum('amount');

            $totalExpenseMonth3 = Expense::whereMonth('date','03')->whereYear('date',$yearFilter)->get();
            $mar = $totalExpenseMonth3->sum('amount');

            $totalExpenseMonth4 = Expense::whereMonth('date','04')->whereYear('date',$yearFilter)->get();
            $apr = $totalExpenseMonth4->sum('amount');

            $totalExpenseMonth5 = Expense::whereMonth('date','05')->whereYear('date',$yearFilter)->get();
            $may = $totalExpenseMonth5->sum('amount');

            $totalExpenseMonth6 = Expense::whereMonth('date','06')->whereYear('date',$yearFilter)->get();
            $jun = $totalExpenseMonth6->sum('amount');

            $totalExpenseMonth7 = Expense::whereMonth('date','07')->whereYear('date',$yearFilter)->get();
            $jly = $totalExpenseMonth7->sum('amount');

            $totalExpenseMonth8 = Expense::whereMonth('date','08')->whereYear('date',$yearFilter)->get();
            $aug = $totalExpenseMonth8->sum('amount');

            $totalExpenseMonth9 = Expense::whereMonth('date','09')->whereYear('date',$yearFilter)->get();
            $sep = $totalExpenseMonth9->sum('amount');

            $totalExpenseMonth10 = Expense::whereMonth('date','10')->whereYear('date',$yearFilter)->get();
            $oct = $totalExpenseMonth10->sum('amount');

            $totalExpenseMonth11 = Expense::whereMonth('date','11')->whereYear('date',$yearFilter)->get();
            $nov = $totalExpenseMonth11->sum('amount');

            $totalExpenseMonth12 = Expense::whereMonth('date','12')->whereYear('date',$yearFilter)->get();
            $dec = $totalExpenseMonth12->sum('amount');


            $totalYear2019 = Expense::whereYear('date','2019')->get(); 
            $totalYear2019 = $totalYear2019->sum('amount');

            $totalYear = Expense::whereYear('date','2020')->get();
            $totalYear = $totalYear->sum('amount');

            $totalYear2021 = Expense::whereYear('date','2021')->get(); 
            $totalYear2021 = $totalYear2021->sum('amount');

            $totalYear2022 = Expense::whereYear('date','2022')->get(); 
            $totalYear2022 = $totalYear2022->sum('amount');

            $totalYear2023 = Expense::whereYear('date','2023')->get(); 
            $totalYear2023 = $totalYear2023->sum('amount');

        }else{
            $totalExpenseMonth1 = Expense::whereMonth('date','01')->whereYear('date',$yearFilter)->get();
            $jan = $totalExpenseMonth1->sum('amount');

            $totalExpenseMonth2 = Expense::whereMonth('date','02')->whereYear('date',$yearFilter)->get();
            $feb = $totalExpenseMonth2->sum('amount');

            $totalExpenseMonth3 = Expense::whereMonth('date','03')->whereYear('date',$yearFilter)->get();
            $mar = $totalExpenseMonth3->sum('amount');

            $totalExpenseMonth4 = Expense::whereMonth('date','04')->whereYear('date',$yearFilter)->get();
            $apr = $totalExpenseMonth4->sum('amount');

            $totalExpenseMonth5 = Expense::whereMonth('date','05')->whereYear('date',$yearFilter)->get();
            $may = $totalExpenseMonth5->sum('amount');

            $totalExpenseMonth6 = Expense::whereMonth('date','06')->whereYear('date',$yearFilter)->get();
            $jun = $totalExpenseMonth6->sum('amount');

            $totalExpenseMonth7 = Expense::whereMonth('date','07')->whereYear('date',$yearFilter)->get();
            $jly = $totalExpenseMonth7->sum('amount');

            $totalExpenseMonth8 = Expense::whereMonth('date','08')->whereYear('date',$yearFilter)->get();
            $aug = $totalExpenseMonth8->sum('amount');

            $totalExpenseMonth9 = Expense::whereMonth('date','09')->whereYear('date',$yearFilter)->get();
            $sep = $totalExpenseMonth9->sum('amount');

            $totalExpenseMonth10 = Expense::whereMonth('date','10')->whereYear('date',$yearFilter)->get();
            $oct = $totalExpenseMonth10->sum('amount');

            $totalExpenseMonth11 = Expense::whereMonth('date','11')->whereYear('date',$yearFilter)->get();
            $nov = $totalExpenseMonth11->sum('amount');
            
            $totalExpenseMonth12 = Expense::whereMonth('date','12')->whereYear('date',$yearFilter)->get();
            $dec = $totalExpenseMonth12->sum('amount');

            $totalYear2019 = Expense::whereYear('date','2019')->get(); 
            $totalYear2019 = $totalYear2019->sum('amount');

            $totalYear = Expense::whereYear('date','2020')->get();
            $totalYear = $totalYear->sum('amount');

            $totalYear2021 = Expense::whereYear('date','2021')->get(); 
            $totalYear2021 = $totalYear2021->sum('amount');

            $totalYear2022 = Expense::whereYear('date','2022')->get(); 
            $totalYear2022 = $totalYear2022->sum('amount');

            $totalYear2023 = Expense::whereYear('date','2023')->get(); 
            $totalYear2023 = $totalYear2023->sum('amount');

    }
     return response()->json([
            'jan' => $jan,
            'feb' => $feb,
            'mar' => $mar,
            'apr' => $apr,
            'may' => $may,
            'jun' => $jun,
            'july' => $jly,
            'august' => $aug,
            'sep' => $sep,
            'oct' => $oct,
            'nov' => $nov,
            'dec' => $dec,
            'totalyear2019'=> $totalYear2019,
            'totalyear2020'=> $totalYear,
            'totalyear2021'=> $totalYear2021,
            'totalyear2022'=> $totalYear2022,
            'totalyear2023'=> $totalYear2023,]
            ,200);
		
	}

    public function getAllPlan(){
        $plan = Budget::all();
        return response()->json(['plan'=>$plan],200);
    }

    public function summaryReport(Request $request){
        $yearFilter = $request->input('year');

        $expenseBudget = Expense::all();
        $budgets = Budget::all();

        if(empty($yearFilter)){
          $yearFilter = '2020';

          $incomesYear = Income::whereYear('date',$yearFilter)->sum('amount');
          $expensesYear = Expense::whereYear('date',$yearFilter)->sum('amount');

        }else{

          $incomesYear = Income::whereYear('date',$yearFilter)->sum('amount');
          $expensesYear = Expense::whereYear('date',$yearFilter)->sum('amount');

        }
        return response()->json([
            'difference'=>$incomesYear - $expensesYear,
            'expense'=>$expensesYear,
            'income' => $incomesYear
        ],200);
    }
}
