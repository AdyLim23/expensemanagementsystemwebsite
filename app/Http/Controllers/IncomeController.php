<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Income;
use Carbon\Carbon;

class IncomeController extends Controller
{
    public function create()
    {
        
    }
    
    public function store(Request $request)
    {
        $this->validate($request,[
            // 'category'=>'required',
            'amount' =>'required',
            'date'  =>'required',
            'description' => 'required',
        ]);
        $income = new Income;
        $income->amount = $request->input('amount');
        $income->date = Carbon::parse($request->input('date'));
        $income->description = $request->input('description');
        $income->user_id = auth()->id();

        $income->save();
        info('Entered');
        return redirect('income')->with('success','Data Saved');
    }
    
    public function index()
    {
        info('Entered');
        $incomes = Income::all();

        info('Print ' . $incomes);

        return view('income', [
        'incomes' => $incomes
        ]);
    
    }
    
    public function show($id)
    {
        
    }
    
    public function edit($id)
    {
    
    }
    
    public function update(Request $request, $id)
    {
        $this->validate($request,[
    
            'amount' =>'required',
            'date'  =>'required',
            'description' => 'required',
        ]);
        
        $income = Income::find($id);
        // if(!$expense) throw new ModelNotFoundException;
        $income->amount = $request->input('amount');
        $income->date = Carbon::parse($request->input('date'));
        $income->description = $request->input('description');

        $income->save();
        return redirect('income')->with('success','Data Updated');
    }

    public function destroy($id)
    {
        $income = Income::find($id);
      
        if($income != null){
        $income->delete();
        
        return redirect('income')->with('success','Data has been deleted');
        }

    }
}
