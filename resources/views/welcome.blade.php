<html>
    <head>

        <title>Smart Expense</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <link rel="stylesheet" type="text/css" href="{{ url('/css/style.css') }}" />
        {{-- Font Awesome --}}
        <link rel="stylesheet" type="text/css" href="{{ asset('css/app.css') }}">
    </head>

    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>
                        <a href="{{ route('register') }}">Register</a>
                    @endauth
                </div>
            @endif

            <div class="content positionIcon-title">
                <div>
                    <img src="/images/savingsIcon.png" class="savingIcon">
                </div>
                <div class="title m-b-md " style="color:white;font-weight: 600;font-size:20px;">
                    Smart Expense
                </div>

                <div style="color:white;padding-top:50px;padding-bottom:50px;">
                    <h1>DISCOVER A BETTER WAY TO MANAGE SPEND.</h1>
                    <i>Happiness included</i>
                </div>

                <div style="text-align:center;padding:30px 20% 0px 20%;">
                    <div class="column">
                        <img src="images/expense.png" class="iconFirstPage" >
                        <p style="color:white;">Expense</p>
                        <p style="color:white;">Create and submit expenses daily and easily from your phone or desktop—and gain robust reporting and compliance controls.
                        </p>
                    </div>

                    <div class="column">
                        <img src="images/budget.png" class="iconFirstPage">
                        <p style="color:white;">Budget</p>
                        <p style="color:white;">Select the recommendation plan or create your own budget for help to increase the chances of savings
                        </p>
                    </div>

                    <div class="column">
                        <img src="images/sync.png" class="iconFirstPage">
                        <p style="color:white;">Sync</p>
                        <p style="color:white;">Provide the sync feature for two platforms which are website and android phone.Track expenses more convenience
                        </p>
                    </div>

                    <div class="column">
                        <img src="images/document.png" class="iconFirstPage">
                        <p style="color:white;">Document</p>
                        <p style="color:white;">Provide document about the savings knowledge
                        </p>
                    </div>

              {{--   <div class="links">
                    <a href="https://laravel.com/docs">Documentation</a>
                    <a href="https://laracasts.com">Laracasts</a>
                    <a href="https://laravel-news.com">News</a>
                    <a href="https://forge.laravel.com">Forge</a>
                    <a href="https://github.com/laravel/laravel">GitHub</a>
                </div> --}}



            </div>
        </div>
    </body>
</html>
