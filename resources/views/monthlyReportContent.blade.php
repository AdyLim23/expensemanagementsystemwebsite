@extends('layouts.monthlyReport')

@section('content')
   <?php 
      $expenseBudget = App\Expense::all();
      $budgets = App\Budget::all();

      if(empty($yearFilter)){
      $yearFilter = '2020';

      $incomesYear = App\Income::whereYear('date',$yearFilter)->sum('amount');
      $expensesYear = App\Expense::whereYear('date',$yearFilter)->sum('amount');

      }else{

      $incomesYear = App\Income::whereYear('date',$yearFilter)->sum('amount');
      $expensesYear = App\Expense::whereYear('date',$yearFilter)->sum('amount');
      
      }

      ?>
<div id="reportDashBoard"style="font-size:15px;font-family:Arial, Helvetica, sans-serif;border-bottom:1px solid #DDDDDD;padding:0px 15px;color:#818181;padding-left:14%;padding-top:4.5%;" id="expenseMove">Monthly Report

<form method="GET" action="{{action('ReportController@reportShow')}}">  
{{csrf_field()}}
    <input type='text' class="form-control"  name="year" style="width:15%;font-size:12px;background:#fff url(https://cdn1.iconfinder.com/data/icons/cc_mono_icon_set/blacks/16x16/calendar_2.png)  97% 50% no-repeat ;cursor:pointer;margin-left:130px;margin-top:-25px;">

      <button type="submit" class="btn btn-primary" style="font-size:12px;margin-top:-2.1%;margin-left:26%;">Search

      </button>
</form>
</div>

<div id="snackbar">Syncing .....</div>
<div style="padding-top:20px;"></div>

<div style="padding-left:35%;">
    <div class="card" style="border-radius:1.25rem;width:40%;">
        <div class="card-header" style="font-size:15px;color:#5D6E73;">
            Report <span>Year {{$yearFilter}}</span>
        </div>
        <div class="card-body" style="padding-bottom:10px;padding-top:30px;">
            <div style="height:30px;font-size:13px;">
              <p><b>Income (RM)</b><span style="float:right;color:green;font-weight:600;">{{$incomesYear}}</span></p>
              <p><b>Expense(RM)</b><span style="float:right;color:red;font-weight:600;">{{$expensesYear}}</span></p>
              <p><b>Difference(RM)</b><span style="float:right;font-weight:600;">{{$incomesYear-$expensesYear}}</span></p>
            </div>   
        </div>
    </div>
</div>

<div class="container" id="monthlyReport" style="padding-top:30px;">
    <div>
        <div>
            <div class="panel panel-default">
                <div class="panel-heading" style="font-size:15px;background-color:rgba(0,0,0,.03);font-family:Arial, Helvetica, sans-serif">Report

                </div>

                <div class="panel-body">
                    <div>
                        <div>     
                           <canvas id="myChart" style="width:658px;height:329px;"></canvas>
                            <div style="margin-right:-45%;padding-bottom:40px;">
                                <p style="padding-top:20px;padding-left:33%;font-size:12px;"><b>Year : {{$yearFilter}}</b></p>
                            </div>
                        </div>

                        <div>
                            <canvas id="lineChart"></canvas>
                        </div>
        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
