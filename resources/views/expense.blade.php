@extends('layouts.expenseYield')

@section('content')
<div id="expenseDashBoard"style="font-size:15px;font-family:Arial, Helvetica, sans-serif;border-bottom:1px solid #DDDDDD;padding:14px 15px;color:#818181;padding-left:14%;padding-top:4.5%;" id="expenseMove">Expense
   
        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#expenseModal" style="margin-left:15px;"><i class="fa fa-plus" style="padding-right:5px;"></i>Expense</button>
    
</div>
{{-- FOR Message when added EXPENSE --}}
@if(count($errors)>0)

<div class="alert alert-danger" id="failNotificationExpense">
  <ul style="font-size:15px;font-family:Arial, Helvetica, sans-serif;padding-left:13.5%;">
    @foreach($errors->all() as $error)
    <li>{{$error}}</li>
    @endforeach
  </ul>
</div>
@endif

@if(\Session::has('success'))
<div class="alert alert-success" id="successNotificationExpense">
  <p style="font-size:15px;font-family:Arial, Helvetica, sans-serif;padding-left:13.5%;">{{ \Session::get('success')}}</p>
</div>
@endif

<?php 
 $budgets = App\Budget::all();
 $categories = App\Category::all();
?>

<div id="snackbar">Syncing .....</div>
<div class="modal fade" id="expenseModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color:rgba(0,0,0,.03);">
                <h5 class="modal-title" id="exampleModalCenterTitle" style="font-size:15px;font-family:Arial, Helvetica, sans-serif;">Add New Expenses</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
            </div>

        <form action="{{ action('ExpenseController@store')}}" method="POST">
            {{csrf_field()}}
            <div class="modal-body">
                    <div class="form-group row" style="padding-top:10px;padding-bottom:20px;" id="plan-box">
                        <label class="col-sm-3 col-form-label" style="font-size:13px;padding-top:20px;font-weight:600;">Plan Name <span style="font-size:11px;"> (Created)</span>
                        </label>

                        <div class="col-sm-4" style="padding-top:22px;">
                            <select id="inputState" name="budget_id" class="form-control fa" style="font-size:15px;padding-top:2px;width:auto;">
                                @foreach($budgets as $budget)
                            
                                    <option value="{{$budget->id}}">{{$budget->namePlan}}</option>
                            
                                @endforeach
                            </select>

                        </div>
                    </div>

                    <div class="form-group row" style="padding-top:10px;">
                        <label class="col-sm-3 col-form-label" style="font-size:13px;padding-top:7px;font-weight:600;">Categories</label>
                            <div class="form-group col-md-4" style="padding-top:5px;">
                                    <select id="inputState" name="category_id"class="form-control fa" style="font-size:15px;padding-top:2px;width:auto;">
                                        @foreach($categories as $category)
                                            <option value="{{$category->id}}">{{$category->type}} </option>
                                        @endforeach
                                    </select>
                            </div>
                    </div>

                    <div class="form-group row" style="padding-top:10px;">
                        <label class="col-sm-2 col-form-label" style="font-size:13px;padding-top:7px;font-weight:600;">Amount (RM)</label>
                        <div class="col-sm-10" style="width:70%;padding-top:4px;">
                            <input type="number" name="amount" step=".01" class="form-control" style="width:30%;font-size:12px;height:30px;margin-left:10%;">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label" style="font-size:13px;padding-top:25px;font-weight:600;">Date</label>
                        <div class="col-sm-10" style="padding-top:22px;">
                            <input type='text' name="date" class="form-control" data-provide="datepicker" style="width:30%;font-size:12px;background:#fff url(https://cdn1.iconfinder.com/data/icons/cc_mono_icon_set/blacks/16x16/calendar_2.png)  97% 50% no-repeat ;cursor:pointer;margin-left:10%;">
                        </div>
                    </div>

                    <div class="form-group row" style="padding-top:10px;">
                        <label class="col-sm-2 col-form-label" style="font-size:13px;padding-top:25px;font-weight:600;">Description</label>
                            <div class="col-sm-10" style="padding-top:25px;">
                                <textarea class="form-control" style="font-size:12px;width:50%;margin-left:10%;" name="description" rows="3" maxlength="55"></textarea>
                            </div>
                    </div>
                </div>

                <div class="modal-footer" style="margin-bottom:-10px;">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal" style="font-size:12px;">Close</button>
                    <button type="submit" class="btn btn-primary" style="font-size:12px;margin-bottom:2px;">Save changes</button>
                </div>
          </form>
        </div>
    </div>
</div>

<div style="padding-top:20px;"></div>

<div style="padding-left:200px;padding-right:200px;" id="expenseList">
   {{--  <div class="card" style="border-radius:1.25rem;width:90%;">
        <div class="card-header" style="font-size:15px;color:#5D6E73;">
            Expense Lists
        </div>
        <div class="card-body" style="padding-bottom:10px;padding-top:30px;"> --}}
                <table id="datatable" class="table table-hover table-striped" >
                    <thead>
                        <tr>
                          <th scope="col">#</th>
                          <th scope="col">Expense Category</th>
                          <th scope="col">Amount</th>
                          <th scope="col">Date</th>
                          <th scope="col">Description</th>
                          <th scope="col">Action</th>
                        </tr>
                    </thead>
                    
                    <tbody>
                     {{--  @if(!$expenses as $i =>$expense)
                      <tr>
                        <th scope="row">1</th>
                        <td>1</td>
                        <td>1</td>
                        <td>1</td>
                        <td>1</td>
                        <td>1</td>
                      </tr>
                      @else --}}
                      @foreach ($expenses as $i => $expense)
                        <tr>
                          {{-- <input type="text" value="{{$expense->id}}"> --}}
                          <input id="hiddenId" type="hidden" value="{{$expense->id}}" />
                          <th scope="row"> {{$i+1}}</th>
                          <td>{{$expense->category->type}}</td>
                          <td>{{$expense->amount}}</td>
                          <td>{{$expense->date}}</td>
                          <td>{{$expense->description}}</td>
                          <td> 
                            <div style="margin-top:-3px;">
                                <a href="#" class="btn btn-outline-success edit" style="margin-left:2%;" >Edit</a> 
                                <a href="#" class="btn btn-outline-danger delete"  style="margin-left:2%;" >Delete</a>
                            </div>
                          </td>
                        </tr>
                     
                        @endforeach
                        {{-- @endif --}}
                    </tbody>

                </table>
                    {{-- <ul class="pagination justify-content-end" style="font-size:12px;">
                        <li class="page-item disabled">
                          <a class="page-link" href="#" tabindex="-1">Previous</a>
                      </li>
                      <li class="page-item"><a class="page-link" href="#">1</a></li>
                      <li class="page-item"><a class="page-link" href="#">2</a></li>
                      <li class="page-item"><a class="page-link" href="#">3</a></li>
                      <li class="page-item">
                          <a class="page-link" href="#">Next</a>
                      </li>
                    </ul> --}}
       {{--  </div>
    </div> --}}
</div>

{{-- Edit Expense Modal --}}
<div class="modal fade" id="editexpenseModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color:rgba(0,0,0,.03);">
                <h5 class="modal-title" id="exampleModalCenterTitle" style="font-size:15px;font-family:Arial, Helvetica, sans-serif;">Edit Expenses</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
            </div>

          <form action="/expense" method="POST" id="editForm">
            {{ csrf_field() }}
            {{ method_field('PUT') }}

            <div class="modal-body">
                    <div class="form-group row" style="padding-top:10px;">
                        <label class="col-sm-2 col-form-label" style="font-size:12px;padding-top:7px;">Plan</label>
                            <div class="form-group col-md-4" style="padding-top:5px;">
                                    <select id="budget" name="budget_id" class="form-control fa" style="font-size:15px;padding-top:2px;width:auto;">
                                     @foreach($budgets as $budget)

                                      <option value="{{$budget->id}}">{{$budget->namePlan}}</option>

                                     @endforeach
                                    </select>
                            </div>
                    </div>

                    <div class="form-group row" style="padding-top:10px;">
                        <label class="col-sm-2 col-form-label" style="font-size:12px;padding-top:7px;">Categories</label>
                            <div class="form-group col-md-4" style="padding-top:5px;">
                                    <select id="category" name="category_id" class="form-control fa" style="font-size:15px;padding-top:2px;width:auto;">
                                        @foreach($categories as $category)
                                            <option value="{{$category->id}}">{{$category->type}} </option>
                                        @endforeach
                                    </select>
                            </div>
                    </div>

                    <div class="form-group row" style="padding-top:10px;">
                        <label class="col-sm-2 col-form-label" style="font-size:12px;padding-top:7px;">Amount (RM)</label>
                        <div class="col-sm-10" style="width:70%;padding-top:4px;">
                            <input type="number" step=".01" id="amount" name="amount" class="form-control" style="width:30%;font-size:12px;height:30px;"/>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label" style="font-size:12px;padding-top:25px;">Date</label>
                        <div class="col-sm-10" style="padding-top:22px;">
                            <input type='text' id="date" name="date" class="form-control" data-provide="datepicker" style="width:30%;font-size:12px;background:#fff url(https://cdn1.iconfinder.com/data/icons/cc_mono_icon_set/blacks/16x16/calendar_2.png)  97% 50% no-repeat ;cursor:pointer;"/>

                        </div>
                    </div>

                    <div class="form-group row" style="padding-top:10px;">
                        <label  class="col-sm-2 col-form-label" style="font-size:12px;padding-top:25px;">Description</label>
                            <div class="col-sm-10" style="padding-top:25px;">
                                <textarea class="form-control" style="font-size:12px;width:50%;" id="description" name="description" rows="3" maxlength="55"></textarea>
                            </div>
                    </div>
            </div>

            <div class="modal-footer" style="margin-bottom:-10px;">
                <button type="button" class="btn btn-secondary" data-dismiss="modal" style="font-size:12px;">Close</button>
                <button type="submit" class="btn btn-primary" style="font-size:12px;margin-top:0px;">Update</button>
            </div>
        </form>
      </div>
    </div>
</div>

{{-- Delete Expense Modal --}}
<div class="modal fade" id="deleteexpenseModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color:rgba(0,0,0,.03);">
                <h5 class="modal-title" id="exampleModalCenterTitle" style="font-size:15px;font-family:Arial, Helvetica, sans-serif;">Delete Expenses</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
            </div>

          <form action="/expense" method="POST" id="deleteForm">
            {{ csrf_field() }}
            {{ method_field('DELETE') }}

            <div class="modal-body" style="font-size:12px;">
                   <input type="hidden" name="_method" value="DELETE">
                    <p>Are you sure to delete this expense data?</p>
            </div>

            <div class="modal-footer" style="margin-bottom:-10px;">
                <button type="button" class="btn btn-secondary" data-dismiss="modal" style="font-size:12px;">Close</button>
                <button type="submit" class="btn btn-danger" style="font-size:12px;margin-top:0px;">Delete</button>
            </div>
        </form>
      </div>
    </div>
</div>


@endsection
