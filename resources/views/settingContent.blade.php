@extends('layouts.setting')

@section('contents')
<div style="padding-top:8%;padding-left:10%;padding-bottom:0px;padding-right:10%;">
<div class="jumbotron" style="background-color:white;" id="setting">

            <div style="padding-top:20px;">
                @if(empty($user->image))
                <img src="/uploads/avatars/default.jpg" style="height:125px;width:125px;border-radius:60px;margin:auto;display:block;" alt="image">
                @else
                <img src="{{ asset('uploads/avatars/'. $user->image) }}" style="height:125px;width:125px;border-radius:60px;margin:auto;display:block;" alt="image">
                @endif
                {{-- <img src="/uploads/avatars/{{$user->avatar}}" style="height:125px;width:125px;border-radius:60px;margin:auto;display:block;"> --}}

                   {{--  <form enctype="multipart/form-data" action="/setting" method="POST">
                        <input type="file" name="avatar" style="padding-left:45%;padding-top:20px;padding-bottom:20px;">
                        <input type="hidden" name="_token" value="{{ csrf_token()}}">
                        <input type="submit" value="Upload" class="pull-right btn btn-sm btn-primary" style="font-size:12px;margin-right:48%;">
                    </form> --}}

                    <form action="/setting" method="POST" enctype="multipart/form-data">
                        {{csrf_field()}}
                            <input type="file" name="image" style="padding-left:45%;padding-top:20px;padding-bottom:20px;">
                            <input type="submit" value="Upload" class="pull-right btn btn-sm btn-primary" style="font-size:12px;margin-right:48%;">
                    </form>
            </div>

        <div style="width:75%;padding-left:27%;padding-top:40px;" id="yourdetail-Setting">
            <div class="card" style="border-radius:1.25rem;text-align:center;">
                <div class="card-header" style="font-size:20px;color:#5D6E73;">
                    Your Details
                </div>
                <div class="card-body">
                    <p style="font-size:15px;"><b>Username :</b><span> {{ Auth::user()->name }}</span></p>
                    <p style="font-size:15px;"><b>Email Address :</b><span> {{ Auth::user()->email }}</span></p>
                </div>
            </div>
        </div>

        <h3 style="padding-top:25px;padding-left:60px;">General Account Settings</h3>
        <div style="padding:10px 60px 10px 60px;">
            <div class="card" style="border-radius:1.25rem;">
                <div class="card-header" style="font-size:20px;color:#5D6E73;">
                    Login
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-2">
                            <i class="fa fa-key" style="font-size:25px;padding-top:17px;padding-left:25px;"></i>
                        </div>
                        <div class="col-md-4">
                            <h3><b>Change Password</b></h3>
                            <p style="font-size:12px;">It's a good idea to use a strong password that you don't use elsewhere</p>
                        </div>
                        <div class="col-md-4" style="padding-top:20px;">
                             <button type="submit" class="btn btn-outline-success" style="font-size:15px;float:right;margin-right:-50%;" onclick="window.location='{{ url("setting/changePassword") }}'">Change Password</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div style="padding:20px 60px 10px 60px;">
            <div class="card" style="border-radius:1.25rem;">
                <div class="card-header" style="font-size:20px;color:#5D6E73;">
                    Change Username
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-2">
                            <i class="fa fa-user" style="font-size:25px;padding-top:17px;padding-left:25px;"></i>
                        </div>
                        <div class="col-md-4">
                            <h3><b>Change Your Name</b></h3>
                             <p style="font-size:12px;">You can change any awesome name you like by click the button</p>
                        </div>
                        <div class="col-md-4" style="padding-top:20px;">
                           <button type="submit" class="btn btn-outline-success" style="font-size:15px;float:right;margin-right:-50%;" onclick="window.location='{{ url("setting/changeUsername") }}'">Change Name</button>
                       </div>
                   </div>
               </div>
           </div>
       </div>
       

        <div style="padding:20px 60px 10px 60px;">
            <div class="card" style="border-radius:1.25rem;">
                <div class="card-header" style="font-size:20px;color:#5D6E73;">
                    Notification
                </div>
                <div class="card-body">
                   <input type="checkbox" checked data-toggle="toggle">
                </div>
            </div>
        </div>

        <div id="snackbar">Syncing ...</div>
</div>
</div>
@endsection
