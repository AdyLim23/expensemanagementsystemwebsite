@extends('layouts.incomeYield')

@section('content')
<div id="incomeDashBoard"style="font-size:15px;font-family:Arial, Helvetica, sans-serif;border-bottom:1px solid #DDDDDD;padding:14px 15px;color:#818181;padding-left:14%;padding-top:4.5%;" id="expenseMove">Income
   
        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#incomeModal" style="margin-left:15px;"><i class="fa fa-plus" style="padding-right:5px;"></i>Income</button>
    
</div>

{{-- FOR Message when added EXPENSE --}}
@if(count($errors)>0)

<div class="alert alert-danger" id="failNotificationExpense">
  <ul style="font-size:15px;font-family:Arial, Helvetica, sans-serif;padding-left:13.5%;">
    @foreach($errors->all() as $error)
    <li>{{$error}}</li>
    @endforeach
  </ul>
</div>
@endif

@if(\Session::has('success'))
<div class="alert alert-success" id="successNotificationExpense">
  <p style="font-size:15px;font-family:Arial, Helvetica, sans-serif;padding-left:13.5%;">{{ \Session::get('success')}}</p>
</div>
@endif

<div id="snackbar">Syncing .....</div>
<div class="modal fade" id="incomeModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color:rgba(0,0,0,.03);">
                <h5 class="modal-title" id="exampleModalCenterTitle" style="font-size:15px;font-family:Arial, Helvetica, sans-serif;">Add New Income</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
            </div>

        <form action="{{ action('IncomeController@store')}}" method="POST">
            {{csrf_field()}}
            <div class="modal-body">
              
                    <div class="form-group row" style="padding-top:10px;">
                        <label class="col-sm-2 col-form-label" style="font-size:13px;padding-top:7px;font-weight:600;">Amount (RM)</label>
                        <div class="col-sm-10" style="width:70%;padding-top:4px;">
                            <input type="number" name="amount" step=".01" class="form-control" style="width:30%;font-size:12px;height:30px;">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label" style="font-size:13px;padding-top:25px;font-weight:600;">Date</label>
                        <div class="col-sm-10" style="padding-top:22px;">
                            <input type='text' name="date" class="form-control" data-provide="datepicker" style="width:30%;font-size:12px;background:#fff url(https://cdn1.iconfinder.com/data/icons/cc_mono_icon_set/blacks/16x16/calendar_2.png)  97% 50% no-repeat ;cursor:pointer;">
                        </div>
                    </div>

                    <div class="form-group row" style="padding-top:10px;">
                        <label for="exampleFormControlTextarea1" class="col-sm-2 col-form-label" style="font-size:13px;padding-top:25px;font-weight:600;">Description</label>
                            <div class="col-sm-10" style="padding-top:25px;">
                                <textarea class="form-control" name="description" style="font-size:12px;width:50%;" id="exampleFormControlTextarea1" rows="3"></textarea>
                            </div>
                    </div>
               
            </div>

            <div class="modal-footer" style="margin-bottom:-10px;">
                <button type="button" class="btn btn-secondary" data-dismiss="modal" style="font-size:12px;">Close</button>
                <button type="submit" class="btn btn-primary" style="font-size:12px;margin-top:0px;">Save changes</button>
            </div>

          </form>

        </div>
    </div>
</div>
<div style="padding-top:20px;"></div>

<div style="padding-left:200px;padding-right:200px;" id="incomeList">
    {{-- <div class="card" style="border-radius:1.25rem;width:90%;">
        <div class="card-header" style="font-size:15px;color:#5D6E73;">
            Income Lists
        </div>
        <div class="card-body" style="padding-bottom:10px;padding-top:30px;"> --}}
                <table id="datatable" class="table table-hover table-striped" >
                    <thead>
                        <tr>
                          <th scope="col">#</th>
                          <th scope="col">Amount</th>
                          <th scope="col">Date</th>
                          <th scope="col">Description</th>
                          <th scope="col">Action</th>
                        </tr>
                    </thead>

                    <tbody>
                      @foreach ($incomes as $i => $income)
                        <tr>
                            
                          <input type="hidden" id="hiddenId" value="{{$income->id}}">
                          <th scope="row"> {{$i+1}}</th>
                          <td>{{$income->amount}}</td>
                          <td>{{$income->date}}</td>
                          <td>{{$income->description}}</td>
                          <td> 
                            <div style="margin-top:-3px;">
                                <a href="#" class="btn btn-outline-success edit" style="margin-left:2%;" >Edit</a> 
                                <a href="#" class="btn btn-outline-danger delete"  style="margin-left:2%;" >Delete</a>
                            </div>
                          </td>
                        </tr>

                        @endforeach
                        
                    </tbody>

                </table>
      {{--   </div>
    </div> --}}
</div>

<div class="modal fade" id="editincomeModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color:rgba(0,0,0,.03);">
                <h5 class="modal-title" id="exampleModalCenterTitle" style="font-size:15px;font-family:Arial, Helvetica, sans-serif;">Add New Income</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
            </div>


        <form action="/income" method="POST" id="editForm">
            {{ csrf_field() }}
            {{ method_field('PUT') }} 

            <div class="modal-body">
                
                    <div class="form-group row" style="padding-top:10px;">
                        <label class="col-sm-2 col-form-label" style="font-size:12px;padding-top:7px;">Amount (RM)</label>
                        <div class="col-sm-10" style="width:70%;padding-top:4px;">
                            <input type="decimal" step=".01" id="amount" name="amount" class="form-control" style="width:30%;font-size:12px;height:30px;">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label" style="font-size:12px;padding-top:25px;">Date</label>
                        <div class="col-sm-10" style="padding-top:22px;">
                            <input type='text' id="date" name="date" class="form-control" data-provide="datepicker" style="width:30%;font-size:12px;background:#fff url(https://cdn1.iconfinder.com/data/icons/cc_mono_icon_set/blacks/16x16/calendar_2.png)  97% 50% no-repeat ;cursor:pointer;">
                        </div>
                    </div>

                    <div class="form-group row" style="padding-top:10px;">
                        <label for="exampleFormControlTextarea1" class="col-sm-2 col-form-label" style="font-size:12px;padding-top:25px;">Description</label>
                            <div class="col-sm-10" style="padding-top:25px;">
                                <textarea class="form-control" id="description" name="description" style="font-size:12px;width:50%;" id="exampleFormControlTextarea1" rows="3"></textarea>
                            </div>
                    </div>
               
            </div>

            <div class="modal-footer" style="margin-bottom:-10px;">
                <button type="button" class="btn btn-secondary" data-dismiss="modal" style="font-size:12px;">Close</button>
                <button type="submit" class="btn btn-primary" style="font-size:12px;margin-top:-0px;">Save changes</button>
            </div>

        </form>
        </div>
    </div>
</div>

<div class="modal fade" id="deleteincomeModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color:rgba(0,0,0,.03);">
                <h5 class="modal-title" id="exampleModalCenterTitle" style="font-size:15px;font-family:Arial, Helvetica, sans-serif;">Delete Income</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
            </div>

          <form action="/income" method="POST" id="deleteForm">
            {{ csrf_field() }}
            {{ method_field('DELETE') }}

            <div class="modal-body" style="font-size:12px;">
                   <input type="hidden" name="_method" value="DELETE">
                    <p>Are you sure to delete this income data?</p>
            </div>

            <div class="modal-footer" style="margin-bottom:-10px;">
                <button type="button" class="btn btn-secondary" data-dismiss="modal" style="font-size:12px;">Close</button>
                <button type="submit" class="btn btn-danger" style="font-size:12px;margin-top:-0px;">Delete</button>
            </div>
        </form>
      </div>
    </div>
</div>

@endsection
