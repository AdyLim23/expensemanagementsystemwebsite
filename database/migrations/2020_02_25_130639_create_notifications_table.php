<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notifications', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('type');
            $table->morphs('notifiable');
            $table->text('data');
            $table->timestamp('read_at')->nullable();
            $table->timestamps();

            $table->unsignedInteger('expense_id');
            $table->unsignedInteger('income_id');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('budget_id');

            $table->foreign('expense_id')
            ->references('id')->on('expenses');
            
            $table->foreign('income_id')
            ->references('id')->on('incomes');

            $table->foreign('user_id')
            ->references('id')->on('users');
           
            $table->foreign('budget_id')
            ->references('id')->on('budgets');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notifications');
    }
}